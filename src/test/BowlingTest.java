package test;

import main.Bowling;
import main.BowlingGame;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import static org.junit.Assert.*;


public class BowlingTest {

    @Rule
    public Timeout globalTimeout = Timeout.seconds(2);

    private Bowling game;

    @Before
    public void setUp(){
        game = new BowlingGame();
    }

    @Test
    public void gutterGame() {
		runRoll(game, 20, 0);
		assertEquals(0,game.score());
    }

	@Test
	public void allOnesGame() {
		runRoll(game, 20, 1);
		assertEquals(20, game.score());
	}

	@Test
	public void oneSpareGame() {
		game.roll(5);
		game.roll(5);
		game.roll(3);
		runRoll(game, 17, 0);
		assertEquals(16,game.score());
	}

	@Test
	public void notSpareGame() {
		game.roll(1);
		game.roll(5);
		game.roll(5);
		runRoll(game,17,0);
		assertEquals(11,game.score());
	}

	@Test
	public void oneStrikeGame() {
		 // Il test deve simulare una partita in cui avviene uno strike.
		 // Es., lo score di: roll(10), roll(3), roll(4), roll(0), ..., roll(0), è 24.
		game.roll(10);
		game.roll(3);
		game.roll(4);
		runRoll(game, 16,0);
		assertEquals(24,game.score());
	}

	@Test
	public void notStrikeGame() {
		 // Il test deve simulare una partita in cui vengono colpiti 10 birilli con il secondo tiro di un frame,
		 // Questa condizione deve essere riconosciuta come spare e non come strike.
		 // Es., lo score di: roll(0), roll(10), roll(3), roll(0), ..., roll(0), è 16.
		game.roll(0);
		game.roll(10);
		game.roll(3);
		runRoll(game,17,0);
		assertEquals(16,game.score());
	}

	@Test
	public void lastFrameStrikeGame() {
		 // Il test deve simulare una partita in cui avviene uno strike nell'ultimo frame.
		 // In questo caso il giocatore completa il frame ed ha diritto ad un tiro aggiuntivo.
		 // Es., lo score di: roll(0), ..., roll(0), roll(10), roll(3), roll(2), è 15.
		runRoll(game, 18, 0);
		game.roll(10);
		game.roll(3);
		game.roll(2);
		assertEquals(15 , game.score());
	}

	@Test
	public void perfectGame(){
		 // Il test deve simulare una partita perfetta in cui avvengono 12 strike di seguito.
		 // Es., lo score di: roll(10), ..., roll(10), è 300.
		runRoll(game,12,10);
		assertEquals(300,game.score());
	}

	@Test
	public void noPerfectGame(){
    	runRoll(game,11,10);
    	assertEquals(200,game.score());
	}

/**/
	private static void runRoll(Bowling game, int TiriRimasti, int Punteggio) {
		for (int i = 0; i < TiriRimasti; i++)
			game.roll(Punteggio);
	}

}