package main;

public class BowlingGame implements Bowling {
    private static final int MAX_TIRI = 22;
    private static final int EFFETTIVI_PUNTEGGI = MAX_TIRI - 2;
    private int pins[];
    private int i;

    public BowlingGame(){
        pins = new int[MAX_TIRI];
        i = 0;
    }

    @Override
    public void roll(int pins) {
        this.pins[i] += pins;
        if(i >= MAX_TIRI - 2) i++;
        else if(i%2==0 && pins == 10)
            i+=2;
        else i++;
    }

    @Override
    public int score() {
        int risultato=0,i;
        if(checkPerfectGame(pins))
            return 300;
        for(i=0; i< EFFETTIVI_PUNTEGGI; i+=2)
            risultato +=valutaggioTiro(pins,i);
        return risultato;
    }

    private static int valutaggioTiro(int pins[],int i){
        if(i!= EFFETTIVI_PUNTEGGI && pins[i] == 10)
            return pins[i] + pins[i + 2] + pins[i + 3];
        return  valutaggioSpare(pins,i);
    }
    private static int valutaggioSpare(int pins[],int i){
        int appoggio = pins[i] + pins[i+1];
        if (i!= EFFETTIVI_PUNTEGGI && 10 == appoggio)
            return  appoggio + pins[i+2];
        return  appoggio;
    }

    private static boolean checkPerfectGame(int pins[]){
        int i;
        for(i=0;i<MAX_TIRI;i+=2)
            if(pins[i]!=10)
                return false;
        return  pins[i-2] == pins[i-1];
    }
}
