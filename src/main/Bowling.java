package main;

public interface Bowling {
    public void roll(int pins);
    public int score();
}